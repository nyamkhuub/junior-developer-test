<?php

//include header section
include './assets/layout/header.php';

require "vendor/autoload.php";

use Products\Model\Product;

ini_set('display_errors', 1);
error_reporting(~0);
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$selected = isset($_POST['selected']) ? $_POST['selected'] : [];
	$type = $_POST['actionName'];
	if ($type == 1) {
		Product::delete([]);
	} else if ($type == 2 && isset($_POST['selected'])) {
		$deleteProducts = array();
		foreach($selected as $key => $value) {
			array_push($deleteProducts, $key);
		}
		Product::delete($deleteProducts);
	}
}
?>

<div class="container">
	<form method="POST">
		<div class="table_header">
			<h3>Product List</h3>
			<div>
				<select name="actionName">
					<option value="1">Mass delete action</option>
					<option value="2">Delete only checked</option>
				</select>
				<input type="submit" value="Apply" />
				<a href="add_product.php">Add new product</a>
			</div>
		</div>
		<hr>
		<div class="table_body">
			<?php
				$products = Product::get();
				foreach($products as $product):?>
				<div class="table_element">
				<input type="checkbox" name="selected[<?php echo $product->getSku(); ?>]" />
					<center>
						<h5><?php echo $product->getSku(); ?></h5>
						<h3><?php echo $product->getName(); ?><h3>
						<h5><?php echo $product->getPrice().' $'; ?></h5>
						<h5><?php echo $product->getType().': '.$product->getSize(); ?></h5>
					</center>
				</div>
			<?php endforeach ?>
		</div>
	</form>
</div>
</body>
</html>
