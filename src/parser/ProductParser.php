<?php

namespace Products\Parser;

use Products\Model\Product;
use Products\Model\ProductType;
use Products\Model\ProductUnit;

class ProductParser {
	private $sku;
	private $name;
	private $price;
	private $type;
	private $size;
	private $weight;
	private $height;
	private $width;
	private $length;

	public function __construct($sku, $name, $price,
		$type, $size, $weight, $height, $width, $length) {
		$this->sku = $sku;
		$this->name = $name;
		$this->price = $price;
		$this->type = $type;
		$this->size = $size;
		$this->weight = $weight;
		$this->height = $height;
		$this->width = $width;
		$this->length = $length;
	}

	/**
	 * This function purpose is process request parameters and create object of Product.
	 * @return Products\Model\Product object.
	 */
	public function get() {
		$product = new Product();
		$product->setSku($this->sku);
		$product->setName($this->name);
		$product->setPrice($this->price);
		$product->setType($this->type);
		if ($this->type == ProductType::BOOK) {
			$product->setSize($this->weight.' '.ProductUnit::BOOK);
		} else if ($this->type == ProductType::DVD) {
			$product->setSize($this->size.' '.ProductUnit::DVD);
		} else {
			$product->setSize($this->height.'x'.$this->width.'x'.$this->length);
		}
		return $product;
	}
}
