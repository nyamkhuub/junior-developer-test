<?php

namespace Products\Config;

use \PDO as pdo;

/**
 * Database connection informations are crucial such as database name, database password.
 * So we need to store into local environment variable. For example, export DB_HOST=localhost.
 */
class Database extends pdo {
	private	$dbHost;
	private $dbUser;
	private $dbName;
	private $dbPassword;

	/**
	 * try to connect database by using these parameters.
	 * 
	 * in this case, I want to use PDO connection. There are other methods to connect database.
	 * But from my study I have to use this method.
	 * @link https://phpbestpractices.org/
	 * But I can't pass PDO connection instance to other classes.
	 * So I decided to create class that is extend from PDO class.
	 */
	public function __construct() {
		$this->dbHost = getenv('DB_HOST') ? getenv('DB_HOST') : "localhost:8889";
		$this->dbName = getenv('DB_NAME') ? getenv('DB_NAME') : "products_junior_test";
		$this->dbUser = getenv('DB_USER') ? getenv('DB_USER') : "root";
		$this->dbPassword = getenv('DB_PASSWORD') ? getenv('DB_PASSOWORD') : "root";
		$host = 'mysql:host='.$this->dbHost.';dbname='.$this->dbName;
		parent::__construct($host, $this->dbUser, $this->dbPassword);
	}
}
