<?php
namespace Products\Model;

use Products\Config\Database;

class Product extends Model {
	/**
	 * unique for each product
	 */
	private $sku;
	private $name;
	private $price;
	private $size;
	/**
	 * type of product.
	 */
	private $type;

	public function getSku() {
		return $this->sku;
	}
	public function getName() {
		return $this->name;
	}
	public function getPrice() {
		return $this->price;
	}
	public function getSize() {
		return $this->size;
	}
	public function getType() {
		return $this->type;
	}
	public function setSku($sku) {
		$this->sku = $sku;
	}
	public function setName($name) {
		$this->name = $name;
	}
	public function setPrice($price) {
		$this->price = $price;
	}
	public function setType($type) {
		$this->type = $type;
	}
	public function setSize($size) {
		$this->size = $size;
	}
	public function toString() {
		return $this->sku.' '.$this->name.' '.$this->price.' '.$this->size;
	}

	/**
	 * read data that is stored in the database and return a list of Product class objects
	 */
	public static function get() {
		$db = new Database();
		$result = $db->query('select * from products')->fetchAll(\PDO::FETCH_CLASS, 'Products\Model\Product');
		return $result;
	}

	/**
	 * @param $product instance of Product class
	 * insert new data into database by using form data
	 */
	public static function create($product) {
		$query = "insert into products values(?, ?, ?, ?, ?)";
		$db = new Database();
		$stmt = $db->prepare($query);
		$stmt->execute([$product->getSku(), $product->getName(), $product->getPrice(),
			$product->getSize(), $product->getType()]);
	}
	/**
	 * @param $products list of checked products sku values.
	 * delete function has two main actions.
	 * if parameter length is equal to zero, the function will delete all data that is stored in the database.
	 * Otherwise, the function will delete according to the values in the parameter.
	 */
	public static function delete($products) {
		$query = "delete from products";
		$db = new Database();
		if (count($products) == 0) {
			$db->query($query);
		} else {
			$params = str_repeat("?", count($products));
			$query = $query." where sku in (".$params.")";
			$stmt = $db->prepare($query);
			$stmt->execute($products);
		}
	}
}
