<?php

namespace Products\Model;

abstract class Model {
	/**
	 * read data which is stored in database and return list of product objects.
	 */
	abstract public static function get();
	abstract public static function create($product);
	abstract public static function delete($products);
}
