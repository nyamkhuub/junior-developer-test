<?php

namespace Products\Model;


abstract class ProductType {
	const BOOK = 'Book';
	const DVD = 'DVD-disc';
	const FURNITURE = 'Furniture';
}
