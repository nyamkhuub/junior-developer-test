<?php

namespace Products\Model;

abstract class ProductUnit {
	const BOOK = 'KG';
	const DVD = 'MB';	
}
