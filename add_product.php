<?php

//include header section
include './assets/layout/header.php';

require "vendor/autoload.php";

use Products\Model\Product;
use Products\Parser\ProductParser;

/**
 * When press apply button, doesn't show any html element also error.
 *
 * Here is solution.
 * @link https://stackoverflow.com/questions/5680831/php-does-not-display-error-messages
 */
ini_set('display_errors', 1);
error_reporting(~0);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$size = isset($_POST['size'])? $_POST['size'] : '';
	$weight = isset($_POST['weight'])? $_POST['weight'] : '';
	$height = isset($_POST['height'])? $_POST['height'] : '';
	$width = isset($_POST['width'])? $_POST['width'] : '';
	$length = isset($_POST['length'])? $_POST['length'] : '';
	$parser = new ProductParser($_POST['sku'], $_POST['name'], $_POST['price'],
		$_POST['type'], $size, $weight, $height, $width, $length);

	$prod = $parser->get();

	Product::create($prod);
}
?>

<div class="container">
	<form method="POST">
		<div class="table_header">
			<h3>Add new product</h3>
			<div>
				<input type="submit" value="Submit" />
			</div>
		</div>
		<hr>
		<div class="table_body">
			<ul>
				<li><h3>Sku: </h3><input type="text" name="sku" placeholder="For example: JVK200123" required/></li>
				<li><h3>Name: </h3><input type="text" name="name" placeholder="For example: Rich dad Poor dad" required/></li>
				<li>
					<h3>Price: </h3>
					<ul id="price">
						<li>
							<ul>
								<li>
									<input id="price-input" type="text" name="price" placeholder="For example: 100, 4.5 etc..." required/>	
								</li>
								<li>
									<h5>$<h5>
								</li>
							</ul>
						</li>
						<li><h5 id="price-warning">You put only number for example 100, 20.5</h5></li>
					</ul>
				<li>
					<h3>Type switcher: </h3>
					<select id="type_switcher" name="type">
						<option>Book</option>
						<option>DVD-disc</option>
						<option>Furniture</option>
					</select>
				</li>
				<li>
					<div id="special"></div>
				</li>
			</ul>
		</div>
	</form>
</div>
<script src ="assets/js/add_product.js"></script>
</body>
</html>
