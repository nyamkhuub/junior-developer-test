# Junior developer test
This is my own version of your company junior developer test.

## Built with
MAMP 5.5  
MySQL 5.7.26  
PHP 7.3.8  
Javascipt  
Sass

## Screenshots
![Products Screen](/screenshots/product-list.png?raw=true "Products list screen")
![Add products Screen](/screenshots/add-product.png?raw=true "add product screen")
