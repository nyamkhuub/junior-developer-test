//this is my own regular expression but doesn't work
//var digitChecker = /[0-9]*\.[0-9]|[0-9]*/;
//Here is solution
//@link https://stackoverflow.com/questions/10256061/regular-expression-for-finding-decimal-float-numbers
var digitChecker = /^[+-]?\d+(\.\d+)?$/;
var selectType = document.getElementById("type_switcher");
var specialSection = document.getElementById("special");
var priceInput = document.getElementById("price-input");
priceInput.addEventListener("change", priceChangeEvent);
selectType.addEventListener("change", selectChangeEvent);

selectType.value = "Book";
bookSection();
function priceChangeEvent() {
	validate(this.value, this, 'price-warning');	
}

function selectChangeEvent() {
	switch(selectType.value) {
		case "Book":
			bookSection();
			break;
		case "DVD-disc":
			dvdDiscSection()
			break;
		case "Furniture":
			furnitureSection();
			break;
	}
}

function clearChild(section) {
	while(section.lastElementChild) {
		section.removeChild(section.lastElementChild);
	}
}

function createTitle(name) {
	var text = document.createElement("h3");
	text.innerHTML = name;
	return text;
}

function validate(value, own, idWarning) {
	if (!value.match(digitChecker)) {
		own.value = null;
		var example = document.getElementById(idWarning);
		example.className = 'warning';
		setTimeout(function() {
			example.className = '';
		}, 1500);
	}
}

function createInput(name, placeHolder) {
	var input = document.createElement("input");
	input.required = true;
	input.name = name;
	input.placeholder = placeHolder;
	input.onchange = function() {
		validate(this.value, this, 'example');
	};
	return input;
}

function createExample(text) {
	var h5text = document.createElement("h5");
	h5text.innerHTML = text;
	h5text.id = 'example';
	return h5text;
}

function bookSection() {
	var h3Text = createTitle("Weight:");	
	var bookInput = inputWithUnit("weight", "For example: 1, 1.2 or 4", "kg");
	var example = createExample("you can put into only number.");
	clearChild(specialSection);
	specialSection.appendChild(h3Text);
	specialSection.appendChild(bookInput);
	specialSection.appendChild(example);
}

function dvdDiscSection() {
	var sizeTitle = createTitle("Size:");
	var sizeInput = inputWithUnit("size", "For example 1.3, 10", "MB");
	var example = createExample("you can put into only number.");

	clearChild(specialSection);
	specialSection.appendChild(sizeTitle);
	specialSection.appendChild(sizeInput);
	specialSection.appendChild(example);
}

function inputWithUnit(name, placeHolder, unit) {
	var parentDiv = document.createElement("ul");
	var inputLi = document.createElement("li");
	var input = createInput(name, placeHolder);
	inputLi.appendChild(input);
	var unitLi = document.createElement("li");
	var unitElement = document.createElement("h5");
	unitElement.innerHTML = unit;
	unitLi.appendChild(unitElement);

	parentDiv.appendChild(inputLi);
	parentDiv.appendChild(unitLi);
	return parentDiv;
}

function furnitureSection() {
	var height = createTitle("Height:");
	var heightInput = inputWithUnit("height", "For example: 100, 200", "cm");
	var width = createTitle("Width:");
	var widthInput = inputWithUnit("width", "For example: 100, 200", "cm");
	var length = createTitle("Length:");
	var lengthInput = inputWithUnit("length", "For example: 100, 200", "cm");
	var warning = createExample("You can put into only number. Please provide dimensions in HxWxL format.");

	clearChild(specialSection);
	specialSection.appendChild(height);
	specialSection.appendChild(heightInput);
	specialSection.appendChild(width);
	specialSection.appendChild(widthInput);
	specialSection.appendChild(length);
	specialSection.appendChild(lengthInput);
	specialSection.appendChild(warning);
}

