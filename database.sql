--CREATE DATABASE
create database products_junior_test;

create table products(
	sku varchar(10) not null,
	name varchar(255) not null,
	price decimal(10, 2) not null,
	size varchar(20) not null,
	type varchar(11) not null,
	-- or unique key(sku)
	primary key(sku)
);
